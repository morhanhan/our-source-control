local anim8 = require 'anim8'
local image, animation

function love.load()
  
  
  
  background= love.graphics.newImage("sprites/bg.png")
  backgroundQuad = love.graphics.newQuad(1,1,720/2,1280/2,720/2,1280/2)
  
  flappy = love.graphics.newImage("sprites/flappy.png")
  flappyPosY = 200
  flappyPosX = 125
  
  pipe = love.graphics.newImage("sprites/pipe.png")
  pipePosX = 300
  pipePosY = 500
  
  gameOver = love.graphics.newImage("sprites/gameOver.png")
  gameOverPosY = love.graphics.getHeight( ) 
  gameOverPosX = love.graphics.getWidth( ) 
  
  
  
  --loads spritesheet
  image = love.graphics.newImage('sprites/Dudon.png')
  local g = anim8.newGrid(95, 90, image:getWidth(), image:getHeight())
  animation = anim8.newAnimation(g('1-6','1-2' , '1-3','3-4'), 0.1)
  animationPosX = 100
  animationPosY = 200
  
  mousePosX=0 
  mousePosY=0
  mouseEndPosX = 0
  mouseEndPosY = 0
  
  movingLeft =false
  movingRight =false
  movingUp=false
  movingDown=false
  
end

--movement input controls here
function love.update(dt)
  
 animation:update(dt)
  
  if love.keyboard.isDown("up") then
		 flappyPosY =  flappyPosY -5.0
  end
  
  if love.keyboard.isDown("left") then
      pipePosX = pipePosX +5.0
  end    
  
  if love.keyboard.isDown("right") then
      pipePosX = pipePosX -5.0
  end    
  
  
  if pipePosX<0-pipe:getWidth() then
		 pipePosX =  love.graphics.getWidth( )
     pipePosY = love.math.random(love.graphics.getHeight())
  end
  
  if pipePosX>love.graphics.getWidth( )+1.0 then
		 pipePosX =  0-pipe:getWidth()
     pipePosY = love.math.random(love.graphics.getHeight())
  end
  
  
  if love.mouse.isDown(1) then
    mousePosX = love.mouse.getX()
    mousePosY = love.mouse.getY()

  
  
  if mousePosX>animationPosX then
       movingRight = true
  else
    movingRight = false
  end
  if movingRight == true then
    moveRight()
    end
  
  
  if mousePosX<animationPosX then
     movingLeft =true 
   else
     movingLeft =false
  end   
  if movingLeft ==true then
    moveLeft()
    end
  
  if mousePosY>animationPosY then
     movingDown =true
   else 
     movingDown =false
  end   
  if movingDown ==true then
    moveDown()
    end
  
  if mousePosY<animationPosY then
      movingUp =true
    else
      movingUp =false
  end   
  if movingUp ==true then
    moveUp()
  end
  end
  
  
  
  
 if love.mouse.isDown(1) == false and movingRight==true then
  pipePosX = pipePosX +(0.02)
  end
  if love.mouse.isDown(1) == false and movingLeft==true then
  pipePosX = pipePosX -(0.02)
end
if love.mouse.isDown(1) == false and movingUp==true then
  animationPosY = animationPosY +(0.02)
end
if love.mouse.isDown(1) == false and movingDown==true then
  animationPosY = animationPosY -(0.02)
  end
  
end


function moveRight()
  pipePosX = pipePosX -(0.1*(mousePosX-animationPosX)) 
  end
function moveLeft()
  pipePosX = pipePosX +(0.1*(animationPosX-mousePosX))
  end
function moveUp()
  animationPosY = animationPosY -(0.1*(animationPosY-mousePosY))
  end
function moveDown()
   animationPosY = animationPosY +(0.1*(mousePosY-animationPosY))
  end
  
function love.draw()

  love.graphics.draw(background, backgroundQuad, 0, 0)
  
  animation:draw(image, animationPosX, animationPosY, 0.0, 0.75, 0.75)
  
  game_screen()
end


function game_screen()
  love.graphics.draw(pipe, pipePosX, pipePosY)
  --love.graphics.draw(flappy, flappyPosX, flappyPosY)
  love.graphics.draw(gameOver, gameOverPosX, gameOverPosY)

  pipePosX = pipePosX 
  flappyPosY = flappyPosY 
  hitTest = CheckCollision(pipePosX, pipePosY, 60, 150, animationPosX, animationPosY, 45, 47)
  if(hitTest) then
    -- Do collision stuff
    gameOverPosX=0 + (love.graphics.getWidth( )- 300)/2
    gameOverPosY=love.graphics.getHeight( )/2 -150
  end
  
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end
